from os import listdir
from os.path import isfile, join
import numpy as np
import pdb
mypath = '../Results/'
result_files = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
cent_results_file = mypath+'cent_results.csv'
all_results = np.zeros([len(result_files),9])
pdb.set_trace()
result_count = 0
for filename in result_files:
    name_chunks = filename.split('_')
    if len(name_chunks)>5 and name_chunks[2][:3] == 'lam':
        data = np.loadtxt(mypath+filename,delimiter=',')
        # features needed: C, l1/l2, acc[0-4]
        try:
            lambda1 = float(name_chunks[3])
            lambda2 = float(name_chunks[5])
            num_users = int(name_chunks[1].split('users')[1])
            step_size = float(name_chunks[-1].replace('.csv',''))
            all_results[result_count,0] = num_users
            all_results[result_count,1] = lambda1
            all_results[result_count,2] = lambda2
            all_results[result_count,3] = step_size
            all_results[result_count,4:9] = data[0]
            result_count+=1
        except:
            print 'File: ' + filename + ' is not a LSA result file'


all_results = all_results[:result_count,:]
np.savetxt(cent_results_file,all_results,delimiter=',')

import numpy as np
import itertools


# cate_var: column vector
# input assumption: 
def cate2mhot(cate_var):
    cate_list = np.unique(cate_var)
    num_cate = cate_list.shape[0]
    num_sample = cate_var.shape[0]
    n = 2
    m=1
    c_m_n = 2
    
    while (c_m_n <= num_cate):
        n = n+1
        m = n/2
        c_m_n = 1.0 * np.math.factorial(n) / np.math.factorial(m)/np.math.factorial(n-m)
        
    
    init_list = [1]*m + [0]*m
    combination_list = itertools.combinations(range(n),m)
    comb_ind = [np.array(x) for x in combination_list]
    m_hot_var = np.zeros([num_cate,n])
    
    for c_ind, position in enumerate(comb_ind):
        if (c_ind < num_cate):
            m_hot_var[c_ind,position] = 1
        else:
            break
        
    
    var_coded = np.zeros([num_sample, n])
    
    for cate_ind, cate_val in enumerate(cate_list):
        sample_ind = np.where(cate_var == cate_val)
        var_coded[sample_ind,:] = m_hot_var[cate_ind,:]    
    
    
    return var_coded

      

    
    

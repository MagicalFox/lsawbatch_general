from os import listdir
from os.path import isfile, join
import numpy as np
import pdb
mypath = '../results/marketing/'
result_files = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
cent_baseline_filename_l1 = mypath+'cent_baseline_l1.csv'
cent_baseline_filename_l2 = mypath+'cent_baseline_l2.csv'
all_results_l1 = np.zeros([len(result_files),7])
all_results_l2 = np.zeros([len(result_files),7])
pdb.set_trace()
result_count_l1 = 0
result_count_l2 = 0
for filename in result_files:
    name_chunks = filename.split('_')
    if len(name_chunks)>5 and name_chunks[3] == 'C':
        data = np.loadtxt(mypath+filename,delimiter=',')
        # features needed: C, l1/l2, acc[0-4]
        if 'l1' in filename:
            try:
                C = float(name_chunks[4])
                num_users = int(name_chunks[2])
                all_results_l1[result_count_l1,0] = C
                all_results_l1[result_count_l1,1] = num_users
                all_results_l1[result_count_l1,2:7] = data
                result_count_l1+=1
            except:
                print 'File: ' + filename + 'is not a baseline result file'
        else:
            try:
                C = float(name_chunks[4])
                num_users = int(name_chunks[2])
                all_results_l2[result_count_l2,0] = C
                all_results_l2[result_count_l2,1] = num_users
                all_results_l2[result_count_l2,2:7] = data
                result_count_l2+=1
            except:
                print 'File: ' + filename + 'is not a baseline result file'


all_results_l1 = all_results_l1[:result_count_l1,:]
all_results_l2 = all_results_l2[:result_count_l2,:]
np.savetxt(cent_baseline_filename_l1,all_results_l1,delimiter=',')
np.savetxt(cent_baseline_filename_l2,all_results_l2,delimiter=',')

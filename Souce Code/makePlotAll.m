clc
close all

addpath ../Results/
% load('cent_baseline_l1.csv')
% 
% data = cent_baseline_l1;
% num_user_list = unique(data(:,2));
% best_results = zeros(length(num_user_list),4);
% for i = 1:length(num_user_list)
%     result = data(data(:,2)==num_user_list(i),:);
%     [best_acc,best_acc_ind] = max(mean(result(:,3:7),2));
%     C = result(best_acc_ind,1);
%     std_of_best_acc = std(result(best_acc_ind,3:7));
%     best_results(i,:) = [C,num_user_list(i),best_acc,std_of_best_acc];
% end
% baseline_l1_best_acc = best_results;
% 
% 
% load('cent_baseline_l2.csv')
% 
% data = cent_baseline_l2;
% num_user_list = unique(data(:,2));
% best_results = zeros(length(num_user_list),4);
% for i = 1:length(num_user_list)
%     result = data(data(:,2)==num_user_list(i),:);
%     [best_acc,best_acc_ind] = max(mean(result(:,3:7),2));
%     C = result(best_acc_ind,1);
%     std_of_best_acc = std(result(best_acc_ind,3:7));
%     best_results(i,:) = [C,num_user_list(i),best_acc,std_of_best_acc];
% end
% baseline_l2_best_acc = best_results;

load('cent_results.csv')

data = cent_results;
num_user_list = unique(data(:,1));
best_results = zeros(length(num_user_list),6);
for i = 1:length(num_user_list)
    result = data(data(:,1)==num_user_list(i),:);
    [best_acc,best_acc_ind] = max(mean(result(:,5:9),2));
    lambda1 = result(best_acc_ind,2);
    lambda2 = result(best_acc_ind,3);
    step_size = result(best_acc_ind,4);
    std_of_best_acc = std(result(best_acc_ind,5:9));
    best_results(i,:) = [num_user_list(i),lambda1,lambda2,step_size,best_acc,std_of_best_acc];
end

x = log(num_user_list)/log(2);
h = figure
hold on
errorbar(x,best_results(:,5),best_results(:,6),'g')
errorbar(x,baseline_l1_best_acc(:,3),baseline_l1_best_acc(:,4),'-.r')
errorbar(x,baseline_l2_best_acc(:,3),baseline_l2_best_acc(:,4),'-ob')
xlabel('Number of Users (Log2 Scale)', 'fontsize', 14)
ylabel('Accuracy', 'fontsize', 14)
title('Number of Users v.s. Accuracy (Log2 Scale)', 'fontsize', 16)
legend('LSR','Baseline with L1','Baseline with L2')
%axis([3,13,0.5,0.95])
set(gca,'FontSize',14)
hold off



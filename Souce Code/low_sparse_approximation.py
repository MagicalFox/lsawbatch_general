#-----------------------------------------------------------------------------
#               Low Rank and Sparse Matrix Factorization
#               Author: Yanxin Pan
#               Date: Dec. 20, 2014
#               Notes: This is the code used for learning a preference model  
#                      with the link matrix = low rank matrix + sparse matrix
#-----------------------------------------------------------------------------

import numpy as np
from scipy.stats import logistic

def soft_thresholding(S, lambda2, t):
    '''
    Proximal operator for l1 norm
    '''
    
    prox_S = np.fmax(np.fabs(S) - lambda2 * t, 0) * np.sign(S)
    
    return prox_S

def matrix_shrinkage(L, lambda1, t):
    '''
    Proximal operator for nuclear norm term
    Required a svd function
    '''
    U, Sigma, V = np.linalg.svd(L)
    Sigma_shrink = np.diag(soft_thresholding(Sigma, lambda1, t))
    if U.shape[0] > V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0] - V.shape[0], V.shape[0]))),axis =0)
    elif U.shape[0] < V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0], V.shape[0]-U.shape[0]))),axis =1)
    else:
        Sigma_hat =  Sigma_shrink   
    prox_L =  np.dot(np.dot(U,Sigma_hat),V) 
    
    return prox_L
    
    
def L_update(L_k, t, grad_k, lambda1):
    '''
    Update L
    '''
    L_temp = L_k - t * grad_k
    L_new = matrix_shrinkage(L_temp, lambda1, t)
    return L_new
    
    
def S_update(S_k, t, grad_k, lambda2):
    '''
    Update S
    '''
    S_temp = S_k - t * grad_k
    S_new = soft_thresholding(S_temp, lambda2, t)
    return S_new


def gradient(train_x_mini,train_t_mini,omega):
    x = np.dot(train_x_mini, omega) 
    logit_term = logistic.cdf(x)
    res = np.dot(train_x_mini.T, (logit_term-train_t_mini))
    res /= train_x_mini.shape[0]
    return(res)
    
    
    
def validation(valid_x, valid_t,omega):
    x = np.dot(valid_x, omega)
    predict = np.where(logistic.cdf(x)>0.5,1,0)
    accuracy = np.where(predict == valid_t, 1, 0)
    return(1.0*np.sum(accuracy)/valid_t.shape[0])
    

'''
    
def g_prox(L_k,S_k, grad_k, L_new, S_new):

        g_prox = g(L_k, S_k) - t grad_k 

        
def step_size(L_k, S_k, grad_k, constant):

    t_0 = 1
    L_new = L_update(L_k)
    S_new = S_update(S_k)
    g_new = g(L_new, S_new)
    g_prox = 

'''


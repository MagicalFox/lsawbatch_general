hold on
errorbar(x,best_results(:,5),best_results(:,6),'g')
x
errorbar(x,baseline_l1_best_acc(:,3),baseline_l1_best_acc(:,4),'-.r')
errorbar(x,baseline_l2_best_acc(:,3),baseline_l2_best_acc(:,4),'-ob')
xlabel('Number of Users (Log2 Scale)')
ylabel('Accuracy')
title('Number of Users v.s. Accuracy (Log2 Scale)')
legend('LSR','Baseline with L1','Baseline with L2')
hold off
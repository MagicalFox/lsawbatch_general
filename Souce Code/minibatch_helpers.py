#------------------------------------------------------------------------------
#               Minibatch Classification Helpers
#               Car Market Feature Learning
#               Author: Alex Burnap
#               Date: Sept. 17, 2014
#               Notes: Mini-batch through 1.3 million data points
#               Multiprocess on the parameter search
#------------------------------------------------------------------------------
import numpy as np
from numpy.random import RandomState

def create_random_choice_data( choice_data_str, valid_size, test_size, num_cars, indices_dir, seed=None, savetxt=False):
    """
    Given a randomly selected true (user,car) from 6556, generates a random
    permutation of choice pairs of size [6556*212, 4], (user, car1, car2, label)
    """
    if seed is None:
        raise ValueError('Could not find random seed')

    r = RandomState( seed )
    purchase_choices = np.loadtxt(choice_data_str, delimiter=',').astype(int)
    choice_pairs = [(user, pur, nonpur, 1) if r.randint(0,2)==1 else (user, nonpur, pur, 0)
                         for (user, pur) in enumerate(purchase_choices) 
                         for nonpur in range(num_cars) if nonpur != pur]  
    choice_data_full = r.permutation(choice_pairs)
    
    train_choice_data = choice_data_full[:-(valid_size+test_size)]
    valid_choice_data = choice_data_full[-(valid_size+test_size):-test_size]
    test_choice_data = choice_data_full[-test_size:]

    if savetxt is True:
        np.savetxt(indices_dir + "full_choice_seed_%s.csv" % seed, choice_data_full, delimiter=',', fmt="%i")
        np.savetxt(indices_dir + "train_choice_seed_%s.csv" % seed, train_choice_data, delimiter=',', fmt="%i")
        np.savetxt(indices_dir + "valid_choice_seed_%s.csv" % seed, valid_choice_data, delimiter=',', fmt="%i")
        np.savetxt(indices_dir + "test_choice_seed_%s.csv" % seed, test_choice_data, delimiter=',', fmt="%i")
    else:
        return choice_data_full

def create_minibatch( choice_data_mini, design_features, user_features ):
    """
    Create a matrix of size [len(choice_data_mini),
    num_design_features * num_user_features + num_design_features]
    """
    num_user_features = np.shape(user_features)[1]
    num_design_features = np.shape(design_features)[1]
    num_combined_features = num_user_features*num_design_features + num_design_features
    batch_size = np.shape(choice_data_mini)[0]
    train_x_mini = np.empty([batch_size, num_combined_features])
    train_t_mini = np.empty([batch_size, 1])
    for index, choice_row in enumerate(choice_data_mini):
        user = user_features[choice_data_mini[index, 0], :]
        design_a = design_features[choice_data_mini[index, 1], :]
        design_b = design_features[choice_data_mini[index, 2], :]
        featuresA = np.concatenate((np.outer(user, design_a).reshape((1, num_user_features*num_design_features)),
                                    design_a.reshape(1, num_design_features)), axis=1)
        featuresB = np.concatenate((np.outer(user, design_b).reshape((1, num_user_features*num_design_features)),
                                    design_b.reshape(1, num_design_features)), axis=1)
        train_x_mini[index, :] = featuresA - featuresB
        train_t_mini[index, 0] = choice_row[3]

    return train_x_mini, train_t_mini.ravel()

def minibatcher( choice_data_train, batch_size, design_features, user_features ):
    """Generator of minibatches."""
    num_batches = choice_data_train.shape[0]/batch_size
    index = range(num_batches*batch_size)
    from random import shuffle
    shuffle(index)
    for i in xrange(num_batches):
        batch_ids = index[i*batch_size: (i+1)*batch_size]
        choice_data_mini = choice_data_train[batch_ids, :]
        print "working on batch %s" % i
        train_x_mini, train_t_mini = create_minibatch( choice_data_mini, design_features, user_features )
        print "successfully yielded train_x_mini from minibatcher of size %s" % train_x_mini.shape[0]
        yield train_x_mini, train_t_mini

def train_classifier(clf, train_x, train_t, batch_num, clf_num):
    print "Training classifier %s" % clf_num
    if batch_num == 1:
        clf.fit(train_x, train_t.ravel())    
    else:
        clf.partial_fit(train_x, train_t.ravel(), classes =[0, 1])

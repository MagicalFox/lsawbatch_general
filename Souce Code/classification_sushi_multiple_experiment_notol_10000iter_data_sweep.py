#------------------------------------------------------------------------------
#               Classification with low rank and sparsity structure
#               Author: Yanxin Pan
#               Date: Jan. 5, 2015
#               Refactor: Alex Burnap, Jan. 29, 2015
#               Notes: 
#------------------------------------------------------------------------------
import time
import os
import getopt
import sys
import numpy as np
from sklearn import linear_model, cross_validation, preprocessing
from numpy.random import RandomState
from sklearn.externals import joblib
from minibatch_helpers import create_random_choice_data, create_minibatch, minibatcher, train_classifier
from low_sparse_approximation import gradient, validation, S_update, L_update, soft_thresholding, matrix_shrinkage
from scipy.stats import logistic
import pdb
import random

def main(parameters):
    #------------------ GLOBAL VARIABLES -----------------------------------------
    lambda1 = parameters[0] # default nuclear norm ---- low rank
    lambda2 = parameters[1] # default l1 norm ---- sparsity
    t = parameters[2] # default stepsize
    max_iter =parameters[3]
    make_new_data = False
    batch_size = 225000 # we use full data but shuffle and reindex for smaller exp sizes 
    stop_rule = 0.00000001 # default convergence criteria stop_rule
    cv_test_ratio = 0.75
    train_val_ratio = 2.0/3
    seed_vector = range(1,6)
    indices_dir = "../../Data/LSA/data/indices_shuffle_correct/"
    results_clf_dir = "../Results/best_clf_attributes/"
    results_dir = "../Results/"
    num_users_list = [2**x for x in range(4,13)]
    accuracies = np.zeros([len(num_users_list), len(seed_vector)])
    baseline_accuracies = np.zeros([len(num_users_list), len(seed_vector)])


    #------------------ GET EXTERNAL ARG IN --------------------------------------
    '''
    try:
        opts, args=getopt.getopt(sys.argv[1:], "hl:L:t:b:s:e:",
                ["lambda1=","lambda2=","step_size=","batch_size=","stop_rule=","max_iter="])
    except getopt.GetoptError:
        print 'gae_demo.py -i <inputfile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt =='-h':
            print 'classification_lsaw.py -l1 <lambda1> -l2 <lambda2> -t <step_size> -T <test_size> -b <batch_size> -s <stop_rule> -e <max_iter>'
        elif opt in("-l", "--lambda1="):
            lambda1 = float(arg)
        elif opt in("-L", "--lambda2="):
            lambda2 = float(arg)
        elif opt in("-t","--step_size="):
            t = float(arg)
        elif opt in("-b","--batch_size="):
            batch_size = int(arg)
        elif opt in("-s","--stop_rule="):
            stop_rule = float(arg)
        elif opt in("-e","--max_iter="):
            max_iter = int(arg)
    '''
    #------------------ GET RAW DATA ---------------------------------------------
    user_variables = np.loadtxt("../../Data/LSA/data/sushi/processed_data/cate_user_variables.csv", delimiter=',')
    item_variables = np.loadtxt("../../Data/LSA/data/sushi/processed_data/cate_item_variables.csv", delimiter=',')
    item_variables = item_variables[:10,:]
    item_variables = np.array(item_variables, dtype=float)
    order = np.loadtxt("../../Data/LSA/data/sushi/sushi3a.5000.10.order") # These are the labels
    order = order[:,2:]

    #------------------ GET DATA SIZES -------------------------------------------
    num_user_variables = user_variables.shape[1]
    num_item_variables = item_variables.shape[1]

    #------------------ INITIALIZE L AND S ---------------------------------------
    prev_L = np.zeros(((num_user_variables+1), num_item_variables))
    prev_S = np.zeros(((num_user_variables+1), num_item_variables))
    prev_omega = prev_L

    #------------------ CHECK DIRECTORIES ----------------------------------------
    if not os.path.exists( results_clf_dir ):
        os.makedirs( results_clf_dir )
        
    if not os.path.exists( indices_dir):
        os.makedirs(indices_dir)

    #------------------ DEFINE CHOICE DATA FUNCTIONS -----------------------------
    def make_choice(ind,order):
        # iterate through pairs of items twice
        num_item = order.shape[1]

        ind = np.array(ind)
        num_user = ind.shape[0]

        choice_data = np.zeros([ind.shape[0]*num_item*(num_item-1),4])
        counter = 0

        for i in range(num_item):
            for j in range(num_item):
                if not i==j:
                    choice_data[counter*num_user:(counter+1)*num_user,0] = ind
                    choice_data[counter*num_user:(counter+1)*num_user,1] = i*np.ones(num_user)
                    choice_data[counter*num_user:(counter+1)*num_user,2] = j*np.ones(num_user)
                    counter += 1

        for i in range(choice_data.shape[0]):
            # if order[user_id,item1_id]<order[user_id,item2_id] set choice_data[i,3] to 1
            if find(choice_data[i,1],order[choice_data[i,0],:]) < find(choice_data[i,2],order[choice_data[i,0],:]):
                choice_data[i,3] = 1
        
        index = range(choice_data.shape[0])
        random.shuffle(index)
        choice_data = choice_data[index,:]
        return choice_data

    def find(x,arr):
        for ind, item in enumerate(arr):
            if item == x:
                return ind

    #------------------ BEGIN MULTIPLE DATA SWEEP --------------------------------
    #   Note that we run 9 data sizes from 16 - 4096 users
    #   Each data size has 5 random seeds to split data
    #   Each random seed runs a full baseline and LSA classifier
    #-----------------------------------------------------------------------------
    for data_size_ind, num_users in enumerate(num_users_list):
        print "Number of user are %d meaning %d datapoints" % (num_users, num_users*45)

        for exp_num, seed in enumerate(seed_vector):
            '''
            Note that we are creating seed files for choices for num users here
            '''
            print "Using seed %d" % seed
            random.seed(seed)
            choice_file = indices_dir+"choice_file"+str(seed)+"_num_user"+str(num_users)+".npy"
            if not os.path.exists(choice_file):
                '''
		train_ind = range(num_users)
                random.shuffle(train_ind) # TODO put random seed to change shuffle
                train_ind = train_ind[:num_users]
                test_ind = train_ind[int(cv_test_ratio*len(train_ind)+1):]
                train_ind = train_ind[:int(cv_test_ratio*len(train_ind))]
                val_ind = train_ind[int(train_val_ratio*len(train_ind)+1):]
                train_ind = train_ind[:int(train_val_ratio*len(train_ind))]
                train_choice_data = make_choice(train_ind,order)
                valid_choice_data = make_choice(val_ind,order)
                test_choice_data = make_choice(test_ind,order)
                np.save(choice_file,[train_choice_data,valid_choice_data,test_choice_data])
		'''
            else:
                choice_dict = np.load(choice_file)
                train_choice_data = choice_dict[0]
                valid_choice_data = choice_dict[1]
                test_choice_data = choice_dict[2]
            #------------------ Get Variables and Features -------------------------
            unique_user_inds = np.unique(train_choice_data[:,[0]])
            unique_design_inds = np.unique(train_choice_data[:,[1,2]])
            scaler_users = preprocessing.StandardScaler()
            scaler_designs = preprocessing.StandardScaler()
            scaler_users.fit(user_variables[unique_user_inds.astype(int), :])
            scaler_designs.fit(item_variables[ unique_design_inds.astype(int), :])
            user_features = scaler_users.transform(user_variables)
            design_features = scaler_designs.transform(item_variables)
            print "------Loaded all data..."

            #------------------ CREATE TRAINING AND VALIDATION DATA --------------
            train_x, train_t = create_minibatch( train_choice_data, design_features, user_features)
            valid_x, valid_t = create_minibatch( valid_choice_data, design_features, user_features)

            #------------------ BASELINE CLASSIFIER TRAINING ---------------------
            baseline_classifier = linear_model.LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=0.01, fit_intercept=False, intercept_scaling=1, class_weight=None, random_state=None)

            print 'Training baseline classifier'

            baseline_classifier.fit(train_x, train_t)
            valid_s = baseline_classifier.score(valid_x, valid_t)
            baseline_accuracies[data_size_ind, exp_num] = valid_s
            print "Baseline accuracy: %4.8f." % valid_s
        
            prev_omega_v = baseline_classifier.coef_.reshape(train_x.shape[1])
            prev_omega = prev_omega_v.reshape(((num_user_variables+1), num_item_variables))
            prev_L = prev_omega
            print "Baseline L rank: %4.2f" % np.linalg.matrix_rank(prev_L)
        
            print 'Training LSA classifier'
            begin_yanxin_optim_time = time.time()
            prev_time = begin_yanxin_optim_time
            iter_num = 0
            while (iter_num < max_iter):
               
               gradient_v = gradient(train_x,train_t,prev_omega_v)
               gradient_m = gradient_v.reshape(((num_user_variables+1), num_item_variables))

               new_L = L_update(prev_L, t, gradient_m, lambda1)
               new_S = S_update(prev_S,t, gradient_m,lambda2)
               new_omega = new_L + new_S

               iter_num = iter_num + 1
     
               prev_L = new_L
               prev_S = new_S
               prev_omega = new_omega
               prev_omega_v = new_omega.reshape(prev_omega_v.shape)

               if iter_num%500 ==0:
                   print "iter_num: %d for L1 %4.4f and L2 %4.4f" % (iter_num, lambda1, lambda2)
                   cur_time = time.time()
                   print "TIME for 50 iterations: %4.2f" % (cur_time - prev_time)
                   print "Time overall: %4.2f" % (cur_time - begin_yanxin_optim_time)
                   prev_time = cur_time

            omega_v = prev_omega.reshape(train_x.shape[1])
            valid_score = validation(valid_x, valid_t, omega_v)
            accuracies[data_size_ind, exp_num] = valid_score
            print valid_score
            rank_of_matrix = np.linalg.matrix_rank(new_L)
            print rank_of_matrix

        print 'baseline model'
        print np.mean(baseline_accuracies[data_size_ind]) 
        print np.std(baseline_accuracies[data_size_ind])
        print 'lsa model'
        print np.mean(accuracies[data_size_ind]) 
        print np.std(accuracies[data_size_ind])

        #sparsity_num = float(len(np.nonzero(new_S)[0]))/float(220)

        np.savetxt("../Results/sushi_users"+str(num_users)+"_lam1_"+str(lambda1)+"_lam2_"+str(lambda2)+"_iter_"+str(max_iter)+"_step_"+str(t)+".csv", accuracies[data_size_ind], delimiter=",")
        




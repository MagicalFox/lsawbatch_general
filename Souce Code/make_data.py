import numpy as np
from cate2mhotcode import cate2mhot
import pdb
user_variables = np.load("../data/sushi/sushi3_udata.npy")
user_variables = user_variables.astype('float32')
#item_variables = np.genfromtxt("../data/sushi/sushi3.idata", delimiter=' ')
item_variables = np.load("../data/sushi/sushi3_idata.npy")
item_variables = item_variables.astype('float32')
item_variables = item_variables[:10,:]

user_variables = user_variables[:,[0,1,2,3,6]]
user_cate_feature_ind = [3,4]

item_cate_feature_ind = [2]


for i in user_cate_feature_ind:
    user_variables =np.concatenate([user_variables,cate2mhot(user_variables[:,i])],1)

user_variables = np.delete(user_variables,user_cate_feature_ind,1)


for i in item_cate_feature_ind:
    item_variables =np.concatenate([item_variables,cate2mhot(item_variables[:,i])],1)

item_variables = np.delete(item_variables,item_cate_feature_ind,1)

np.save('../data/sushi/sushi3_idata_m_hot.npy',item_variables)
np.save('../data/sushi/sushi3_udata_m_hot.npy',user_variables)
